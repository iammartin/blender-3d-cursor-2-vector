"""
Vector Right-Click Menu: 3D Cursor
+++++++++++++++++++++++++++++++++
"""

import bpy
from bpy.types import Menu

bl_info = {
    "name": "3D Cursor to Vector",
    "description": "",
    "blender": (3, 0, 0),
    "category": "Node",
}

class WM_OT_button_context_3dcursor2vector(bpy.types.Operator):
    """Set vector to 3D cursor position"""
    bl_idname = "wm.button_context_3dcursor2vector"
    bl_label = "Value to 3D Cursor"

    @classmethod
    def poll(cls, context):
        return context.active_object is not None

    def execute(self, context):
        value = getattr(context, "button_pointer", None)
        if value is not None:
            if  value.type == 'VECTOR':
                value.default_value = bpy.context.scene.cursor.location
            if value.type == 'INPUT_VECTOR':
                value.vector = bpy.context.scene.cursor.location

        return {'FINISHED'}
    
class WM_OT_button_context_vector23dcursor(bpy.types.Operator):
    """Set 3D cursor to vector"""
    bl_idname = "wm.button_context_vector23dcursor"
    bl_label = "3D Cursor to vector"

    @classmethod
    def poll(cls, context):
        return context.active_object is not None

    def execute(self, context):
        value = getattr(context, "button_pointer", None)
        if value is not None:
            if  value.type == 'VECTOR':
                 bpy.context.scene.cursor.location = value.default_value
            if value.type == 'INPUT_VECTOR':
                 bpy.context.scene.cursor.location = value.vector

        return {'FINISHED'}

# This class has to be exactly named like that to insert an entry in the right click menu
class WM_MT_button_context(Menu):
    bl_label = "Unused"

    def draw(self, context):
        pass

def menu_func(self, context):
    value = getattr(context, "button_pointer", None)
    if value.type == 'VECTOR' or value.type == 'INPUT_VECTOR': 
        layout = self.layout
        layout.separator()
        layout.operator(WM_OT_button_context_3dcursor2vector.bl_idname)
        layout.operator(WM_OT_button_context_vector23dcursor.bl_idname)


classes = (
    WM_OT_button_context_3dcursor2vector,
    WM_OT_button_context_vector23dcursor,
    WM_MT_button_context,
)

def register():
    for cls in classes:
        bpy.utils.register_class(cls)
    bpy.types.WM_MT_button_context.append(menu_func)

def unregister():
    for cls in classes:
        bpy.utils.unregister_class(cls)
    bpy.types.WM_MT_button_context.remove(menu_func)

if __name__ == "__main__":
    register()